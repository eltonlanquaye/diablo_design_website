
// Play Video on load

(function() {
    /**
     * Video element
     * @type {HTMLElement}
     */
    var video = document.getElementsByTagName("video");
  
    /**
     * Check if video can play, and play it
     */
    video.addEventListener( "canplay", function() {
      video.play();
    });
  })();